(defvar binary "AA")
(defvar bindir "bin")
(defvar srcdir "src")
(defvar cc "clang++")
(defvar cflags
  '("-c"
    "-std=c++11"
    "-Wall"
    "-I" "/usr/include/OGRE"
    "-I" "/usr/include/OGRE/Overlay"))
(defvar debug-cflags
  '("-g"
    "-Og"
    "-D" "_DEBUG"))
(defvar release-cflags
  '("-O3"))
(defvar ldflags
  '("-l" "boost_system"
    "-l" "boost_thread"
    "-l" "boost_atomic"
    "-l" "pthread"
    "-l" "OgreMain"
    "-l" "OgreOverlay"
    "-l" "OIS"
    "-l" "angelscript"
    "-l" "awesomium-1-7"
    "-l" "steam_api"))
