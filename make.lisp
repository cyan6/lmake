(defvar scripts '())
(defmacro defscript (name &body body)
  `(push '(,name ,@body) scripts))

(defscript "clean"
  (clean))
(defscript "debug"
  (cleanp)
  (compile-project :debugging t)
  (link))
(defscript "release"
  (cleanp)
  (compile-project)
  (link))

(defun println (lines)
  (if (listp lines)
    (format t "~{~a~%~}" lines)
    (format t "~a~%" lines)))

(let ((makescript (or (second (member "-makescript" #+sbcl sb-ext:*posix-argv* :test #'equal))
                      (second (member "--makescript" #+sbcl sb-ext:*posix-argv* :test #'equal))
                      "makescript.lisp")))
  (if (probe-file makescript)
    (load makescript)
    (progn (format t "Error: makescript ~a not found.~%" makescript)
           (usage)
           (exit :code 1))))

(defmacro dir-path (dir &body body)
  `(make-pathname :directory `(:relative ,,dir) ,@body))

(defmacro ls (dir &body body)
  `(directory (dir-path ,dir ,@body)))

(defmacro ls-strings (dir &body body)
  (let ((a (gensym)))
    `(loop for ,a in (ls ,dir ,@body)
           collect (princ-to-string ,a))))

(defun scout-src ()
  (labels ((descend (dir)
             (loop for d in (directory (make-pathname :directory dir :name :wild))
                   for c = (pathname-name d)
                   unless c collect d and append (descend (pathname-directory d))))
           (dirs-files (dirs)
             (loop for dir in dirs
                   for files = (directory (make-pathname :directory (pathname-directory dir) :name :wild :type "cpp"))
                   when files append files)))
    (dirs-files (descend `(:relative ,srcdir)))))

(defmacro run (program args)
  #+sbcl `(sb-ext:run-program ,program ,args
                              :search t
                              :wait t
                              :output t))

(defun compile-project (&key debugging)
  (when (and (ensure-directories-exist (dir-path srcdir))
             (ensure-directories-exist (dir-path bindir)))
    (let ((files (scout-src))
          (flags (if debugging
                   (append cflags debug-cflags)
                   (append cflags release-cflags))))
      (loop for file in files
            do (let* ((filestring (princ-to-string file))
                      (output (princ-to-string
                                (dir-path bindir
                                          :name (pathname-name file)
                                          :type "o")))
                      (flags (append flags `("-o" ,output ,filestring))))
                 (unless (probe-file output)
                   (format t "~a~{ ~a~}~%" cc flags)
                   (run cc flags)))))))

(defun link ()
  (let ((args (append (ls-strings bindir :name :wild :type "o")
                      `("-o" ,(princ-to-string (dir-path bindir :name binary)))
                      ldflags)))
    (format t "~a~{ ~a~}~%" cc args)
    (run cc args)))

(defun clean ()
  (loop for file in (ls bindir :name :wild :type "o")
        do (format t "Deleting ~a~%" file)
        do (delete-file file))
  (let ((bin (dir-path bindir :name binary)))
    (when (probe-file bin)
      (format t "Deleting ~a~%" bin)
      (delete-file bin))))

(defun cleanp ()
  (when (y-or-n-p "clean first?") (clean)))

(defun usage ()
  (println `("Usage: sbcl --script make.lisp [-makescript makescriptfile] script"
             ""
             "  if no makescriptfile is given, makescript.lisp is used instead."
             ""
             "The following scripts are available:"
             ,@(or (loop for s in scripts collect (format nil "-~a" (car s)))
                   '("  None found")))))

(defun member-do (list scripts)
  (loop for script in scripts
        for f = (first script)
        when (member f list :test #'equal)
        do (loop for f in `((princ "--> ") (println ,f) ,@(rest script))
                 do (eval f))
        and return f
        finally (usage)))

(defun main (&optional args)
  (let ((argv (or args
                  #+sbcl (rest sb-ext:*posix-argv*))))
    (if argv
      (member-do argv scripts)
      (usage))))

#+sbcl (main)
#-sbcl (println "This implimentation is not supported.")
#-sbcl (when (y-or-n-p "Attempt anyway?") (main))
